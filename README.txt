Project Name:  DFN1006-3 SMD Breakout PCB
Project Date:  2021-05-12

a H-a-D posting https://hackaday.com/2021/05/11/smallest-discrete-transistor-555-timer/ regarding using DFN1006-3 (1 x 0.6mm) transistors to build a DIP-8 555 timer.  That project inspired me to build a KiCAD based breakout PCB for the DFN1006-3 SMD footprint.

Note: example DFN1006-3 KiCAD footprint file came from SnapEDA and as of: 2021-05-13 is untested, ie: no DFN1006-3 SMD parts have been soldered onto the SnapEDA footprint.

Useful URLs:
------------
* https://forum.swmakers.org/viewtopic.php?f=9&t=2461  ; my SW Makers project posting
* https://hackaday.com/2021/05/11/smallest-discrete-transistor-555-timer/




Hardware:
---------
a small 2 layer PCB that can solder a SMD DFN1006-3 based transistor or MOSFET



PCB Versions:
-------------

Version 1.0:
Production Date:  2021-05-??
PCB Details:  ??, 1.6mm, White, 2-layer PCB


Code Information:
-----------------
N/A


Project Journal:
----------------
2021-05-12:  design up a KiCAD based breakout PCB for the DFN1006-3 SMD footprint. Panelise the PCB into fit 1400 in a 10cm x 10cm panel.  


































