EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "DFN1006-3 SMD Breakout PCB"
Date "2021-05-12"
Rev "1.0"
Comp "PRL"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small Rpdwn?
U 1 1 609E8F11
P 4735 2905
AR Path="/609E67B6/609E8F11" Ref="Rpdwn?"  Part="1" 
AR Path="/609E8F11" Ref="Rpdwn1"  Part="1" 
F 0 "Rpdwn1" H 4770 2830 50  0000 L CNN
F 1 "10KR" H 4495 2815 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4735 2905 50  0001 C CNN
F 3 "~" H 4735 2905 50  0001 C CNN
	1    4735 2905
	-1   0    0    1   
$EndComp
$Comp
L Pauls_Symbol_Library:AO3401A Q?
U 1 1 609E8F17
P 4500 3050
AR Path="/609E67B6/609E8F17" Ref="Q?"  Part="1" 
AR Path="/609E8F17" Ref="Q1"  Part="1" 
F 0 "Q1" H 4375 2965 50  0000 L CNN
F 1 "PMZ350UPEYL" H 4670 3170 50  0000 L CNN
F 2 "Pauls_KiCAD_Libraries:DFN100X60X40-3N" H 4700 2975 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3401A.pdf" H 4500 3050 50  0001 L CNN
	1    4500 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 3050 4735 3050
Wire Wire Line
	4735 3005 4735 3050
Connection ~ 4735 3050
Wire Wire Line
	4735 3050 4780 3050
Wire Wire Line
	4980 3050 5010 3050
$Comp
L Device:R_Small Rpup?
U 1 1 609E8F22
P 4735 3200
AR Path="/609E67B6/609E8F22" Ref="Rpup?"  Part="1" 
AR Path="/609E8F22" Ref="Rpup1"  Part="1" 
F 0 "Rpup1" H 4785 3300 50  0000 L CNN
F 1 "10KR" H 4500 3305 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4735 3200 50  0001 C CNN
F 3 "~" H 4735 3200 50  0001 C CNN
	1    4735 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4735 3050 4735 3100
Wire Wire Line
	5055 2950 5030 2950
Wire Wire Line
	5030 2950 5030 2765
Wire Wire Line
	5030 2765 4735 2765
Wire Wire Line
	4400 2765 4400 2850
Wire Wire Line
	4735 2805 4735 2765
Connection ~ 4735 2765
Wire Wire Line
	4735 2765 4400 2765
Wire Wire Line
	5055 3150 5025 3150
Wire Wire Line
	5025 3150 5025 3340
Wire Wire Line
	5025 3340 4735 3340
Wire Wire Line
	4400 3340 4400 3250
Wire Wire Line
	4735 3300 4735 3340
Connection ~ 4735 3340
Wire Wire Line
	4735 3340 4400 3340
$Comp
L Device:R_Small Rg?
U 1 1 609E8F37
P 4880 3050
AR Path="/609E67B6/609E8F37" Ref="Rg?"  Part="1" 
AR Path="/609E8F37" Ref="Rg1"  Part="1" 
F 0 "Rg1" V 4965 2915 50  0000 L CNN
F 1 "150R" V 4810 2890 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4880 3050 50  0001 C CNN
F 3 "~" H 4880 3050 50  0001 C CNN
	1    4880 3050
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 609E8F3D
P 5255 3050
AR Path="/609E67B6/609E8F3D" Ref="J?"  Part="1" 
AR Path="/609E8F3D" Ref="J1"  Part="1" 
F 0 "J1" H 5365 3245 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5450 2885 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5255 3050 50  0001 C CNN
F 3 "~" H 5255 3050 50  0001 C CNN
	1    5255 3050
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_Small D?
U 1 1 609E8F43
P 4240 3055
AR Path="/609E67B6/609E8F43" Ref="D?"  Part="1" 
AR Path="/609E8F43" Ref="D1"  Part="1" 
F 0 "D1" V 4145 2895 50  0000 L CNN
F 1 "Schottky_Small" V 4225 2420 50  0000 L CNN
F 2 "Diode_SMD:D_0805_2012Metric" V 4240 3055 50  0001 C CNN
F 3 "~" V 4240 3055 50  0001 C CNN
	1    4240 3055
	0    1    1    0   
$EndComp
Wire Wire Line
	4240 2955 4240 2765
Wire Wire Line
	4240 2765 4400 2765
Connection ~ 4400 2765
Wire Wire Line
	4240 3155 4240 3340
Wire Wire Line
	4240 3340 4400 3340
Connection ~ 4400 3340
$Comp
L Device:R_Small R?
U 1 1 609E8F4F
P 4875 3635
AR Path="/609E67B6/609E8F4F" Ref="R?"  Part="1" 
AR Path="/609E8F4F" Ref="R1"  Part="1" 
F 0 "R1" V 4945 3590 50  0000 L CNN
F 1 "2K" V 4805 3590 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 4875 3635 50  0001 C CNN
F 3 "~" H 4875 3635 50  0001 C CNN
	1    4875 3635
	0    1    1    0   
$EndComp
$Comp
L Device:LED_Small D?
U 1 1 609E8F55
P 4735 3500
AR Path="/609E67B6/609E8F55" Ref="D?"  Part="1" 
AR Path="/609E8F55" Ref="D2"  Part="1" 
F 0 "D2" V 4680 3340 50  0000 L CNN
F 1 "Red" V 4800 3305 50  0000 L CNN
F 2 "LED_SMD:LED_0402_1005Metric" V 4735 3500 50  0001 C CNN
F 3 "~" V 4735 3500 50  0001 C CNN
	1    4735 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4735 3400 4735 3340
Wire Wire Line
	4735 3600 4735 3635
Wire Wire Line
	4735 3635 4775 3635
Wire Wire Line
	4975 3635 5045 3635
Wire Wire Line
	5045 3635 5045 3075
Wire Wire Line
	5045 3075 5010 3075
Wire Wire Line
	5010 3075 5010 3050
Connection ~ 5010 3050
Wire Wire Line
	5010 3050 5055 3050
Text Notes 5270 3085 0    50   ~ 0
Gate
Text Notes 5265 2985 0    50   ~ 0
Drain/Source
Text Notes 5270 3180 0    50   ~ 0
Source/Drain
Text Notes 4285 2675 0    50   ~ 0
* Fit pull-up resistor for P-MOS\nor pull-down for N-MOS\nQ1 MOSFETs
Text Notes 3695 4040 0    50   ~ 0
Notes:\n----\n* the PMZ350UPEYL, DFN1006-3 MOSFET is an example only\n* LED and Resistor are Optional\n* Schottky Diode is optional
$EndSCHEMATC
